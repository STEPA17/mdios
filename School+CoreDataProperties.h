//
//  School+CoreDataProperties.h
//  MDiOS
//
//  Created by MD-Staff on 06.11.19.
//  Copyright © 2019 MD. All rights reserved.
//
//

#import "School+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface School (CoreDataProperties)

+ (NSFetchRequest<School *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *name;
@property (nullable, nonatomic, copy) NSString *schoolID;
@property (nullable, nonatomic, retain) NSSet<Student *> *students;

@end

@interface School (CoreDataGeneratedAccessors)

- (void)addStudentsObject:(Student *)value;
- (void)removeStudentsObject:(Student *)value;
- (void)addStudents:(NSSet<Student *> *)values;
- (void)removeStudents:(NSSet<Student *> *)values;

@end

NS_ASSUME_NONNULL_END
