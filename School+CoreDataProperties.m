//
//  School+CoreDataProperties.m
//  MDiOS
//
//  Created by MD-Staff on 06.11.19.
//  Copyright © 2019 MD. All rights reserved.
//
//

#import "School+CoreDataProperties.h"

@implementation School (CoreDataProperties)

+ (NSFetchRequest<School *> *)fetchRequest {
	return [NSFetchRequest fetchRequestWithEntityName:@"School"];
}

@dynamic name;
@dynamic schoolID;
@dynamic students;

@end
