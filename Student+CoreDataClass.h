//
//  Student+CoreDataClass.h
//  MDiOS
//
//  Created by MD-Staff on 06.11.19.
//  Copyright © 2019 MD. All rights reserved.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class School;

NS_ASSUME_NONNULL_BEGIN

@interface Student : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "Student+CoreDataProperties.h"
