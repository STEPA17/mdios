//
//  RedButton.h
//  MDiOS
//
//  Created by Dusan Stjepanovic on 10/23/19.
//  Copyright © 2019 MD. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface RedButton : UIButton

@end

NS_ASSUME_NONNULL_END
