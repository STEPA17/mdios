//
//  RedButton.m
//  MDiOS
//
//  Created by Dusan Stjepanovic on 10/23/19.
//  Copyright © 2019 MD. All rights reserved.
//

#import "RedButton.h"

@implementation RedButton

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    self.backgroundColor = UIColor.redColor;
    self.layer.cornerRadius = 5;
}

    
@end
