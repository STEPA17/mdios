//
//  ViewController.m
//  MDiOS
//
//  Created by Dusan Stjepanovic on 10/21/19.
//  Copyright © 2019 MD. All rights reserved.
//

#import "CitiesViewController.h"
#import "RedButton.h"
#import "AddViewController.h"
#import "DPSearchCities.h"

#define CELL_IDENTIFIER @"cellIdentifier"

@interface CitiesViewController () <UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, strong) NSArray<NSDictionary*>* cities;

@end

@implementation CitiesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    self.title = @"Cities";
    [self.tableView registerClass:UITableViewCell.class forCellReuseIdentifier:CELL_IDENTIFIER];
    
    [SREG.dataProvider searchCities:self.searchText];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self.tableView reloadData];
    [NSNotificationCenter.defaultCenter addObserver:self selector:@selector(didRetrieveCities:) name:@"CitiesRetrieved" object:nil];
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.cities.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:CELL_IDENTIFIER forIndexPath:indexPath];
    
    cell.textLabel.text = self.cities[indexPath.row][@"title"];
    
    return cell;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return @"Cities";
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 100;
}

#pragma mark - Notifications

- (void)didRetrieveCities:(NSNotification*)notif
{
    self.cities = notif.userInfo[@"cities"];
    [self.tableView reloadData];
}

#pragma mark - Getters


- (NSArray<NSDictionary *> *)cities
{
    if (!_cities) {
        _cities = NSArray.new;
    }
    return _cities;
}

@end
