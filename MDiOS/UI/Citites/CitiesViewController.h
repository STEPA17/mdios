//
//  ViewController.h
//  MDiOS
//
//  Created by Dusan Stjepanovic on 10/21/19.
//  Copyright © 2019 MD. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CitiesViewController : UIViewController 

@property (nonatomic, strong) NSString* searchText;

@end

