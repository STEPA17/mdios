//
//  ViewController.m
//  MDiOS
//
//  Created by Dusan Stjepanovic on 10/21/19.
//  Copyright © 2019 MD. All rights reserved.
//

#import "CitiesViewController.h"
#import "RedButton.h"
#import "AddViewController.h"
#import "Student+CoreDataClass.h"
#import "School+CoreDataClass.h"
#import "SchoolDao.h"
#import "StudentDao.h"
#import "SchoolModel.h"
#import "DPSearchCities.h"

#define CELL_IDENTIFIER @"cellIdentifier"

@interface CitiesViewController () <UITableViewDataSource, UITableViewDelegate, AddViewControllerDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, strong) NSMutableArray<NSString*>* people;
@property (weak, nonatomic) IBOutlet RedButton *testButton;
@property (nonatomic, strong) StudentDao* studentDao;
@property (nonatomic, strong) SchoolModel* model;
@property (nonatomic, strong) NSArray<NSDictionary*>* cities;
@end

@implementation CitiesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self.testButton setTitle:@"Dodaj" forState:UIControlStateNormal];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    self.title = @"Controller";
    [self.tableView registerClass:UITableViewCell.class forCellReuseIdentifier:CELL_IDENTIFIER];
    
    [SREG.dataProvider searchCities:self.searchText];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self.tableView reloadData];
    [NSNotificationCenter.defaultCenter addObserver:self selector:@selector(didRetrieveCities:) name:@"CitiesRetrieved" object:nil];
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.cities.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:CELL_IDENTIFIER forIndexPath:indexPath];
    
    cell.textLabel.text = self.cities[indexPath.row][@"title"];
    
    return cell;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return @"Cities";
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 100;
}

#pragma mark - Actions

- (IBAction)didTapAdd:(id)sender {
    
    AddViewController* vc = [self.storyboard instantiateViewControllerWithIdentifier:@"AddViewControllerSID"];
    vc.delegate = self;
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - AddViewControllerDelegate

- (void)didAddPerson:(NSString *)name
{
    [self.people addObject:name];
}

#pragma mark - Notifications

- (void)didAddUser:(NSNotification*)notif
{
    [self.model reset];
    [self.tableView reloadData];
}

- (void)didRetrieveCities:(NSNotification*)notif
{
    self.cities = notif.userInfo[@"cities"];
    [self.tableView reloadData];
}

#pragma mark - Getters

- (NSMutableArray<NSString *> *)people
{
    if (!_people)
    {
        _people = self.studentDao.findAll.mutableCopy;
    }
    return _people;
}

- (StudentDao *)studentDao {
    
    if (!_studentDao) {
        _studentDao = StudentDao.new;
    }
    return _studentDao;
}

- (SchoolModel *)model
{
    if (!_model) {
        _model = SchoolModel.new;
    }
    return _model;
}

- (NSArray<NSDictionary *> *)cities
{
    if (!_cities) {
        _cities = NSArray.new;
    }
    return _cities;
}

@end
