//
//  AddViewController.m
//  MDiOS
//
//  Created by MD-Staff on 04.11.19.
//  Copyright © 2019 MD. All rights reserved.
//

#import "AddViewController.h"
#import "RedButton.h"
#import "StudentDao.h"
#import "Student+CoreDataClass.h"
#import "SchoolDao.h"
#import "CitiesViewController.h"

@interface AddViewController () <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet RedButton *addButton;
@property (weak, nonatomic) IBOutlet UITextField *nameTextField;

@end

@implementation AddViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (IBAction)didTapAdd:(id)sender {
    
    if (self.nameTextField.text.length > 0) {
        
        CitiesViewController* vc = [self.storyboard instantiateViewControllerWithIdentifier:@"CitiesViewControllerSID"];
        vc.searchText = self.nameTextField.text;
        [self.navigationController pushViewController:vc animated:YES];
    }
}

@end
