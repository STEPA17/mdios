//
//  UserDao.m
//  MDiOS
//
//  Created by MD-Staff on 04.11.19.
//  Copyright © 2019 MD. All rights reserved.
//

#import "StudentDao.h"

@implementation StudentDao

- (id)init {
    
    if (self = [super init]) {
        
        self.entityName = @"Student";
        self.defaultOrderBy = [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES];
        self.attributeID = @"studentID";
    }
    
    return self;
}

@end
