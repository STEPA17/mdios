//
//  UserDao.h
//  MDiOS
//
//  Created by MD-Staff on 04.11.19.
//  Copyright © 2019 MD. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GenericDao.h"

NS_ASSUME_NONNULL_BEGIN

@interface StudentDao : GenericDao

@end

NS_ASSUME_NONNULL_END
