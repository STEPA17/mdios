//
//  PetDao.m
//  MDiOS
//
//  Created by MD-Staff on 04.11.19.
//  Copyright © 2019 MD. All rights reserved.
//

#import "SchoolDao.h"

@implementation SchoolDao

- (id)init {
    if (self = [super init]) {
        
        self.entityName = @"School";
        self.defaultOrderBy = [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES];
        self.attributeID = @"schoolID";
    }
    return self;
}

@end
