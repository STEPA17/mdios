//
//  ServiceRegistry.h
//  MDiOS
//
//  Created by Dusan Stjepanovic on 8/11/2019.
//  Copyright © 2019 MD. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DataProvider.h"

NS_ASSUME_NONNULL_BEGIN

@interface ServiceRegistry : NSObject

@property (nonatomic, strong) DataProvider* dataProvider;
@property (readonly, strong) NSPersistentContainer *persistentContainer;

@end

NS_ASSUME_NONNULL_END
