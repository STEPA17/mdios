//
//  DAD
//
//  Created by mini on 11/1/17.
//  Copyright © 2017 ITEngine. All rights reserved.
//

#import "DPUnit.h"
#import "AFHTTPSessionManager.h"


@implementation DPUnit

- (void) execute
{
    self.inProgress = YES;
    [self.apiClientJson GET:self.path parameters:self.params progress:nil success:self.successBlock failure:self.failureBlock];
}

- (id) params
{
    return nil;
}

- (NSString*) path
{
    return nil;
}

- (SuccessBlock) successBlock
{
    return ^(NSURLSessionDataTask* task, id responseObject) {
        NSString* dic = (NSString*)responseObject;
//        NSLog(@"SUCCESS: %@",dic);
        self.inProgress = NO;
    };
}

- (FailureBlock) failureBlock
{
    return ^(NSURLSessionDataTask* task, NSError* error) {
        self.inProgress = NO;
    };
}

- (NSString *)apiVersion {
    return @"v1";
}

- (AFHTTPSessionManager*) apiClientJson {
    
    AFHTTPSessionManager* apiClientJson;
    if (!apiClientJson) {
        NSString* pathAPI = @"https://www.metaweather.com/api/";
        apiClientJson = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:pathAPI]];
        apiClientJson.requestSerializer = [AFJSONRequestSerializer serializer];
        [apiClientJson.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        [apiClientJson.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        
        
        apiClientJson.responseSerializer = [AFJSONResponseSerializer serializer];
    }
    
    return apiClientJson;
}

@end
