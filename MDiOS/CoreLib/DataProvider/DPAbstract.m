//
//  DAD
//
//  Created by mini on 11/1/17.
//  Copyright © 2017 ITEngine. All rights reserved.
//

#import "DPAbstract.h"

@implementation DPAbstract

- (NSString*) identifier
{
    if (!_identifier)
    {
        _identifier = NSUUID.UUID.UUIDString;
    }
    return _identifier;
}

- (void) execute
{
    // abstract
}

@end
