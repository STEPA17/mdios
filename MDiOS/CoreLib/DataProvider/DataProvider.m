//
//  DataProvider.m
//  MDiOS
//
//  Created by Dusan Stjepanovic on 8/11/2019.
//  Copyright © 2019 MD. All rights reserved.
//

#import "DataProvider.h"
#import "DPSearchCities.h"

@implementation DataProvider

- (DPAbstract *)searchCities:(NSString *)searchText
{
    DPSearchCities* provider = [[DPSearchCities alloc] initWithSearchText:searchText];
    [provider execute];
    
    return provider;
}

@end
