//
//  DataProvider.h
//  MDiOS
//
//  Created by Dusan Stjepanovic on 8/11/2019.
//  Copyright © 2019 MD. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DPAbstract.h"

NS_ASSUME_NONNULL_BEGIN

@interface DataProvider : NSObject

- (DPAbstract*)searchCities:(NSString*)searchText;

@end

NS_ASSUME_NONNULL_END
