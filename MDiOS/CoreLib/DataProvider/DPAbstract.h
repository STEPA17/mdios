//
//  DAD
//
//  Created by mini on 11/1/17.
//  Copyright © 2017 ITEngine. All rights reserved.
//

#import <Foundation/Foundation.h>

@class AFHTTPSessionManager;

typedef void(^SuccessBlock)(NSURLSessionDataTask*, id);

typedef void(^FailureBlock)(NSURLSessionDataTask*, NSError*);

typedef void(^SuccessCompletionBlock)(id);

typedef void(^FailureCompletionBlock)(NSError*);


@interface DPAbstract : NSObject

@property(nonatomic, assign) BOOL inProgress;

@property(nonatomic, copy) NSString* identifier;
@property(nonatomic, copy) NSString* notificationNameSuccess;
@property(nonatomic, copy) NSString* notificationNameFail;

// Generally don't use these, but instead rely on NSNotifications. Only a few data providers takes these into account.
@property(nonatomic, strong) SuccessCompletionBlock successCompletionBlock;
@property(nonatomic, strong) FailureCompletionBlock failureCompletionBlock;

- (void) execute;

@end
