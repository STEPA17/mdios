//
//  DPSearchCities.m
//  MDiOS
//
//  Created by MD-Staff on 06.11.19.
//  Copyright © 2019 MD. All rights reserved.
//

#import "DPSearchCities.h"

@implementation DPSearchCities

- (instancetype)initWithSearchText:(NSString*)searchText
{
    if (self = [super init]) {
        _searchText = searchText;
    }
    return self;
}

- (NSString *)path
{
    return @"location/search/";
}

- (id)params
{
    return @{
        @"query" : self.searchText
    };
}

- (SuccessBlock) successBlock
{
    return ^(NSURLSessionDataTask* task, id responseObject) {

        [NSNotificationCenter.defaultCenter postNotificationName:@"CitiesRetrieved" object:nil userInfo:@{ @"cities" : responseObject }];
        self.inProgress = NO;
    };
}

- (FailureBlock) failureBlock
{
    return ^(NSURLSessionDataTask* task, NSError* error) {
        
        
        self.inProgress = NO;
    };
}

@end
