//
//  DAD
//
//  Created by mini on 11/1/17.
//  Copyright © 2017 ITEngine. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DPAbstract.h"


@interface DPUnit : DPAbstract

- (NSString*) path;

- (id) params;

- (SuccessBlock) successBlock;

- (FailureBlock) failureBlock;

- (NSString*) apiVersion;

- (AFHTTPSessionManager*) apiClientJson;

@end
