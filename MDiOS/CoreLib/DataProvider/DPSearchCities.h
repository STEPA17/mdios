//
//  DPSearchCities.h
//  MDiOS
//
//  Created by MD-Staff on 06.11.19.
//  Copyright © 2019 MD. All rights reserved.
//

#import "DPUnit.h"

NS_ASSUME_NONNULL_BEGIN

@interface DPSearchCities : DPUnit

@property (nonatomic, strong) NSString* searchText;

- (instancetype)initWithSearchText:(NSString*)searchText;

@end

NS_ASSUME_NONNULL_END
