//
//  AppDelegate.h
//  MDiOS
//
//  Created by Dusan Stjepanovic on 10/21/19.
//  Copyright © 2019 MD. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "ServiceRegistry.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (nonatomic, strong) ServiceRegistry* serviceRegistry;


@end

