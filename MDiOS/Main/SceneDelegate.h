//
//  SceneDelegate.h
//  MDiOS
//
//  Created by Dusan Stjepanovic on 10/21/19.
//  Copyright © 2019 MD. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

