//
//  Student+CoreDataProperties.h
//  MDiOS
//
//  Created by MD-Staff on 06.11.19.
//  Copyright © 2019 MD. All rights reserved.
//
//

#import "Student+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface Student (CoreDataProperties)

+ (NSFetchRequest<Student *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *name;
@property (nullable, nonatomic, copy) NSString *studentID;
@property (nullable, nonatomic, retain) School *school;

@end

NS_ASSUME_NONNULL_END
